<?php

namespace App\DataFixtures;

use App\Entity\Libro;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for($i = 0; $i<5; $i++){
            $libro = new Libro();
            $libro->setTitulo("Libro ".strval($i));
            $libro->setDescripcion("Una pequeña descripcion de mi libro ".strval($i));
            $libro->setPublicacion("Año publicacion ".strval($i));
            $libro->setAutor("Autor de Libro ".strval($i));
            $manager->persist($libro);
            $manager->flush();
        }

    }
}

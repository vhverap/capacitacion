<?php

namespace App\Controller;

use App\Form\LibroFormType;
use App\Entity\Libro;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;

class EditController extends AbstractController
{
    #[Route('/edit/{id}', name: 'app_edit')]
    public function index(Libro $libro, Request $request, ManagerRegistry $doctrine): Response
    {
        $form = $this->createForm(LibroFormType::class, $libro);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $libroedit = $form->getData();
            $em = $doctrine->getManager();
            $em->persist($libroedit);
            $em->flush();
            return $this->redirectToRoute('app_list');
        };
    }
}

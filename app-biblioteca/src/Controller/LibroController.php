<?php

namespace App\Controller;

use App\Entity\Libro;
use App\Form\LibroFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;


class LibroController extends AbstractController
{
    #[Route('/libro', name: 'app_libro')]
    public function index(Request $request, ManagerRegistry $doctrine): Response
    {
        $form = $this->createForm(LibroFormType::class, new Libro());
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $libro = $form->getData();
            $em = $doctrine->getManager();
            $em->persist($libro);
            $em->flush();
            return $this->redirectToRoute('app_list');
        }

        return $this->render('libro/index.html.twig', [
            'form' => $form->createView(),
        ]);

    }
}
